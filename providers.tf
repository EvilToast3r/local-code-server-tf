locals {
  ga_url  = data.onepassword_item.gitea_creds.url
  ga_user = data.onepassword_item.gitea_creds.username
  gl_user = data.onepassword_item.gitlab_creds.username
  gh_user = data.onepassword_item.github_creds.username
}

terraform {
  required_providers {
    onepassword = {
      source  = "1Password/onepassword"
      version = "~> 1.3.0"
    }
    docker = {
      source  = "kreuzwerker/docker"
      version = ">= 2.23.1"
    }
    github = {
      source  = "integrations/github"
      version = "~> 5.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 16.8.1"
    }
    gitea = {
      source  = "go-gitea/gitea"
      version = ">= 0.1.0"
    }
  }
}

data "onepassword_item" "gitlab_creds" {
  vault = local.vault_id
  uuid  = local.gl_token
}

data "onepassword_item" "github_creds" {
  vault = local.vault_id
  uuid  = local.gh_token
}

data "onepassword_item" "gitea_creds" {
  vault = local.vault_id
  uuid  = local.ga_token
}

provider "gitlab" {
  token = data.onepassword_item.gitlab_creds.password
}

provider "github" {
  token = data.onepassword_item.github_creds.password
}

provider "gitea" {
  base_url = "https://${data.onepassword_item.gitea_creds.url}"
  token    = data.onepassword_item.gitea_creds.password
}

provider "onepassword" {
  service_account_token = local.op_token
}

data "external" "user" {
  working_dir = path.cwd
  program = [
    "/bin/bash",
    "-c",
    "jq -n --arg id $(id -u) --arg usr $(whoami) '{id: $id, usr: $usr}'"
  ]
}

variable "runtime" {
  type     = string
  default  = "podman"
  nullable = false
}

provider "docker" {
  host = (
    data.external.user.result.id != "0" ?
    (
      var.runtime != "podman" ?
      "unix:///home/${data.external.user.result.usr}/.docker/desktop/docker.sock" :
      "unix:///run/user/${data.external.user.result.id}/podman/podman.sock"
    )
    :
    (
      var.runtime != "podman" ?
      "unix:///var/run/docker.sock" :
      "unix:///var/run/podman/podman.sock"
    )
  )
}