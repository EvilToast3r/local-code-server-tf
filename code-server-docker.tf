locals {
  theme    = "Tomorrow Night Blue"
  git_reg  = local.ga_url  # <- Change to gl for gitlab, gh for github, ga for gitea
  git_user = local.ga_user # ^ Ditto
}



resource "docker_image" "local-code-server-img" {
  #depends_on = [ terraform_data.build_cs_image ]
  name = "${local.git_reg}/${local.git_user}/local-code-server-tf/local-code-server:latest"
}

resource "random_pet" "code_server_name" {
  separator = "_"
}

resource "random_integer" "code_server_port" {
  min = 8444
  max = 8450
}

resource "tls_private_key" "cs_ed25519" {
  algorithm = "ED25519"
}

resource "onepassword_item" "cs_sudo_login" {
  vault    = local.vault_id
  title    = "Code Server ${random_pet.code_server_name.id} Sudo"
  url      = "http://localhost:${random_integer.code_server_port.result}/"
  category = "login"
  username = "root"
  password_recipe {
    length  = 32
    symbols = true
  }
  section {
    label = "cs_pk"
    field {
      label = "pubKey"
      type  = "STRING"
      value = tls_private_key.cs_ed25519.public_key_openssh
    }
    field {
      label = "pubKey-sha256"
      type  = "STRING"
      value = tls_private_key.cs_ed25519.public_key_fingerprint_sha256
    }
  }
}

resource "docker_container" "codeserver_container" {
  depends_on = [docker_image.local-code-server-img]
  image      = docker_image.local-code-server-img.image_id
  name       = "Code_Server_${random_pet.code_server_name.id}"
  env = [
    "SUDO_PASSWORD=${onepassword_item.cs_sudo_login.password}",
    "PGID=1000",
    "PUID=1000",
    "TZ=America/New_York",
    "DOTNET_ROOT=/config/.dotnet/",
    "GIT_USER=${local.git_user}",
    "GIT_EMAIL=${local.git_email}",
    "VS_THEME=${local.theme}",
    "CONTINUE_DEV=true",
    "POWERSHELL=true",
    "TERRAFORM=true"
  ]
  mounts {
    target = "/config"
    type   = "volume"
  }
  upload {
    file    = "/config/.ssh/id_ed25519.pub"
    content = sensitive(tls_private_key.cs_ed25519.public_key_openssh)
  }
  upload {
    file    = "/config/.ssh/id_ed25519"
    content = sensitive(tls_private_key.cs_ed25519.private_key_openssh)
  }
  upload {
    file    = "/config/.ssh/id_ed25519.sha256"
    content = sensitive(tls_private_key.cs_ed25519.public_key_fingerprint_sha256)
  }
  ports {
    ip       = "127.0.0.1"
    internal = 8443
    external = random_integer.code_server_port.result
  }
}

resource "gitlab_user_sshkey" "code_server_gl" {
  title      = "${local.git_user}@${random_pet.code_server_name.id}.cs"
  key        = tls_private_key.cs_ed25519.public_key_openssh
  expires_at = timeadd(formatdate("YYYY-MM-DD'T'HH:mm:ssZ", timestamp()), "730h")
}

resource "github_user_ssh_key" "code_server_gh" {
  title = "${local.git_user}@${random_pet.code_server_name.id}.cs"
  key   = tls_private_key.cs_ed25519.public_key_openssh
}

resource "gitea_public_key" "code_server_ga" {
  title    = "${local.git_user}@${random_pet.code_server_name.id}.cs"
  key      = tls_private_key.cs_ed25519.public_key_openssh
  username = local.ga_user
}

output "code_server_name" {
  value = random_pet.code_server_name.id
}

output "code_server_url" {
  value = "http://localhost:${random_integer.code_server_port.result}/"
}
